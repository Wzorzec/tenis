import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fullsize-card',
  templateUrl: './fullsize-card.component.html',
  styleUrls: ['./fullsize-card.component.scss']
})
export class FullsizeCardComponent implements OnInit {

  page = {
    image: 'assets/img_parallax.jpg'
  };

  constructor() { }

  ngOnInit() { }

}
