import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullsizeCardComponent } from './fullsize-card.component';

describe('FullsizeCardComponent', () => {
  let component: FullsizeCardComponent;
  let fixture: ComponentFixture<FullsizeCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FullsizeCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullsizeCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
