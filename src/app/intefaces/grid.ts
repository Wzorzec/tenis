export interface Grid {
    color: string;
    cols: number;
    rows: number;
    text: string;
  }
